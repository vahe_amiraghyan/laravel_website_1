<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

class HomePageController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        return view('front.index');
    }
    public function blog()
    {
        return view('front.blog');
    }
    public function services(){
        return view('front.services');
    }
    public function contact(){
        return view('front.contact');
    }
}
