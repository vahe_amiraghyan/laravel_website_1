<!DOCTYPE html>
<head>
    <title>Polygon CSS Website Template</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <!--
    Polygon Template
    https://templatemo.com/tm-400-polygon
    -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/templatemo_misc.css') }}" rel="stylesheet">
    <link href="{{ asset('css/templatemo_style.css') }}" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100,600' rel='stylesheet' type='text/css'>
    <script src="{{asset('js/jquery-1.10.2.min.js')}}"></script>
    <script src="{{asset('js/jquery.lightbox.js')}}"></script>
    <script src="{{asset('js/templatemo_custom.js')}}"></script>
    <script>
        function showhide()
        {
            var div = document.getElementById("newpost");
            if (div.style.display !== "none")
            {
                div.style.display = "none";
            }
            else {
                div.style.display = "block";
            }
        }
    </script>

</head>
<body>
<div class="site-header">
    <div class="main-navigation">
        <div class="responsive_menu">
            <ul>
                <li><a class="show-1 templatemo_home" href="/">Gallery</a></li>
                <li><a class="show-2 templatemo_page2" href="/blog">Our team</a></li>
                <li><a class="show-3 templatemo_page3" href="#">Services</a></li>
                <li><a class="show-5 templatemo_page5" href="/contacs">Contact</a></li>
            </ul>
        </div>
        <div class="container">
            <div class="row templatemo_gallerygap">
                <div class="col-md-12 responsive-menu">
                    <a href="#" class="menu-toggle-btn">
                        <i class="fa fa-bars"></i>
                    </a>
                </div> <!-- /.col-md-12 -->
                <div class="col-md-3 col-sm-12">
                    <a href="#"><img src="/images/templatemo_logo.jpg" alt="Polygon HTML5 Template"></a>
                </div>
                <div class="col-md-9 main_menu">
                    <ul>
                        <li><a class="@if(Route::currentRouteName() === 'home') active @endif show-1 templatemo_home" href="/">
                                <span class="fa fa-camera"></span>
                                Gallery</a></li>
                        <li><a class="@if(Route::currentRouteName() === 'blog') active @endif show-2 templatemo_page2" href="/blog">
                                <span class="fa fa-users"></span>
                                Our team</a></li>
                        <li><a class="@if(Route::currentRouteName() === 'service') active @endif show-2 templatemo_page2show-3 templatemo_page3" href="/services">
                                <span class="fa fa-cogs"></span>
                                Services</a></li>
                        <li><a class="@if(Route::currentRouteName() === 'contact') active @endif show-2 templatemo_page2show-5 templatemo_page5" href="/contact">
                                <span class="fa fa-envelope"></span>
                                Contact</a></li>
                    </ul>
                </div> <!-- /.col-md-12 -->
            </div> <!-- /.row -->
        </div> <!-- /.container -->
    </div> <!-- /.main-navigation -->
</div> <!-- /.site-header -->
<div id="menu-container"></div>
@yield('content')
<!-- footer start -->
<div class="templatemo_footer">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-sm-12">
                <span>Copyright &copy; 2014 Company Name | Design: TemplateMo</span>
            </div>
            <div class="col-md-3 col-sm-12 templatemo_rfooter">
                <a href="#">
                    <div class="hex_footer">
                        <span class="fa fa-facebook"></span>
                    </div>
                </a>
                <a href="#">
                    <div class="hex_footer">
                        <span class="fa fa-twitter"></span>
                    </div>
                </a>
                <a href="#">
                    <div class="hex_footer">
                        <span class="fa fa-linkedin"></span>
                    </div>
                </a>
                <a href="#">
                    <div class="hex_footer">
                        <span class="fa fa-rss"></span>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>
<!-- footer end -->
<script>
    $('.gallery_more').click(function(){
        var $this = $(this);
        $this.toggleClass('gallery_more');
        if($this.hasClass('gallery_more')){
            $this.text('Load More');
        } else {
            $this.text('Load Less');
        }
    });


</script>

</body>
</html>
