@extends('layouts.home')
@section('content')
    <!-- team start -->
    <div >
        <div class="templatemo_ourteam">
            <div class="container templatemo_hexteam">
                <div class="row">
                    <div class="col-md-3 col-sm-4">
                        <div class="hexagon hexagonteam gallery-item">
                            <div class="hexagon-in1">
                                <div class="hexagon-in2" style="background-image: url('/images/team/1.jpg');">
                                    <div class="overlay templatemo_overlay1">
                                        <a href="#fb">
                                            <div class="smallhexagon">
                                                <span class="fa fa-facebook"></span>
                                            </div>
                                        </a>

                                        <a href="#tw">
                                            <div class="smallhexagon">
                                                <span class="fa fa-twitter"></span>
                                            </div>
                                        </a>

                                        <a href="#ln">
                                            <div class="smallhexagon">
                                                <span class="fa fa-linkedin"></span>
                                            </div>
                                        </a>

                                        <a href="#rs">
                                            <div class="smallhexagon">
                                                <span class="fa fa-rss"></span>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="clear"></div>
                                    <div class="overlay templatemo_overlaytxt">Phasellus interdum</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-8 templatemo_servicetxt" >
                        <h2>Free Template</h2>
                        <p>Polygon is free HTML5 template by <span class="blue">template</span><span class="green">mo</span> that can be used for any purpose. You can remove any credit link. Please tell your friends about our website. Credit goes to <a rel="nofollow" href="http://unsplash.com">Unsplash</a> for images used in this template. Feel free to visit <a href="https://fb.com/templatemo" target="_parent">templatemo page</a> on Facebook.</p>
                    </div>
                    <div class="templatemo_servicecol2">
                        <div class="col-md-3 col-sm-4">
                            <div class="hexagon hexagonteam gallery-item">
                                <div class="hexagon-in1">
                                    <div class="hexagon-in2" style="background-image: url('/images/team/2.jpg');">
                                        <div class="overlay templatemo_overlay1">
                                            <a href="#fb">
                                                <div class="smallhexagon">
                                                    <span class="fa fa-facebook"></span>
                                                </div>
                                            </a>
                                            <a href="#tw">
                                                <div class="smallhexagon">
                                                    <span class="fa fa-twitter"></span>
                                                </div>
                                            </a>
                                            <a href="#ln">
                                                <div class="smallhexagon">
                                                    <span class="fa fa-linkedin"></span>
                                                </div>
                                            </a>
                                            <a href="#rs">
                                                <div class="smallhexagon">
                                                    <span class="fa fa-rss"></span>
                                                </div>
                                            </a>
                                        </div>

                                        <div class="clear"></div>
                                        <div class="overlay templatemo_overlaytxt">Cras interdum accumsan diam</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-8 templatemo_servicetxt">
                            <h2>Responsive Design</h2>
                            <p>Please take a look at <a rel="nofollow" href="https://www.toocss.com">Too CSS</a> to see the collection of free website templates for you. This is free CSS website template fully compatible with tablets and mobile phones. Mauris eget neque at sapien faucibus egestas vel vitae mi. Maecenas commodo augue risus, sed placerat neque feugiat vel.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
            <div class="container templatemo_hexteam s_top">
                <div class="row">
                    <div class="col-md-3 col-sm-4">
                        <div class="hexagon hexagonteam gallery-item">
                            <div class="hexagon-in1">
                                <div class="hexagon-in2" style="background-image: url('/images/team/3.jpg');">
                                    <div class="overlay templatemo_overlay1">
                                        <a href="#fb">
                                            <div class="smallhexagon">
                                                <span class="fa fa-facebook"></span>
                                            </div>
                                        </a>
                                        <a href="#tw">
                                            <div class="smallhexagon">
                                                <span class="fa fa-twitter"></span>
                                            </div>
                                        </a>
                                        <a href="#ln">
                                            <div class="smallhexagon">
                                                <span class="fa fa-linkedin"></span>
                                            </div>
                                        </a>
                                        <a href="#rs">
                                            <div class="smallhexagon">
                                                <span class="fa fa-rss"></span>
                                            </div>
                                        </a>
                                    </div>

                                    <div class="clear"></div>
                                    <div class="overlay templatemo_overlaytxt">Morbi pulvinar</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-8 templatemo_servicetxt" >
                        <h2>Mobile Ready</h2>
                        <p>Sed laoreet, enim quis euismod egestas, risus tortor tincidunt lacus, in iaculis mauris lectus at augue. Donec luctus nibh nec ullamcorper feugiat. Phasellus felis urna, lobortis vitae lacus sit amet, tristique consectetur nibh.</p>
                    </div>
                    <div class="templatemo_servicecol2">
                        <div class="col-md-3 col-sm-4">
                            <div class="hexagon hexagonteam gallery-item">
                                <div class="hexagon-in1">
                                    <div class="hexagon-in2" style="background-image: url('/images/team/4.jpg');">
                                        <div class="overlay templatemo_overlay1">
                                            <a href="#fb">
                                                <div class="smallhexagon">
                                                    <span class="fa fa-facebook"></span>
                                                </div>
                                            </a>
                                            <a href="#tw">
                                                <div class="smallhexagon">
                                                    <span class="fa fa-twitter"></span>
                                                </div>
                                            </a>
                                            <a href="#ln">
                                                <div class="smallhexagon">
                                                    <span class="fa fa-linkedin"></span>
                                                </div>
                                            </a>
                                            <a href="#rs">
                                                <div class="smallhexagon">
                                                    <span class="fa fa-rss"></span>
                                                </div>
                                            </a>
                                        </div>

                                        <div class="clear"></div>
                                        <div class="overlay templatemo_overlaytxt">Sed nonummy</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-8 templatemo_servicetxt">
                            <h2>HTML5 CSS3</h2>
                            <p>Phasellus sodales magna orci, id scelerisque lectus faucibus a. Vivamus varius tincidunt sem. Etiam ultricies orci sit amet sem egestas varius vitae at lacus. Nunc blandit elit in mauris semper, id iaculis felis condimentum.</p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!--team end-->
{{--    <div class="clear"></div>--}}
@endsection
