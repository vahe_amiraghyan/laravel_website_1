<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomePageController;

Route::get('/', [HomePageController::class, 'index'])->name('home');
Route::get('/blog', [HomePageController::class, 'blog'])->name('blog');
Route::get('/services', [HomePageController::class, 'services'])->name('service');
Route::get('/contact', [HomePageController::class, 'contact'])->name('contact');

